/*!
 * jQuery Equal Lines - v0.9.2 - 2018-01-29
 * https://bitbucket.org/relishinc/jquery-equal-lines-plugin
 * Copyright (c) 2018 Steve Palmer
 * Licensed MIT
 */

(function ( $ ) {
 
  $.fn.equalLines = function( options ) {
	  
    var 
    	elements = this,
	    settings = $.extend({
		    precision: 		5,
		    debug: 				false,
		    throttling: 	300
	    }, options )
	    ;
        
		elements
			.each(function() {
				
				if ( ! $(this).data('equalLines.initialized') )
				{
					$(this)
						.data('equalLines.initialized', true)
						.wrapInner('<span data-equal-lines-wrapper style="display:inline-block; max-width: 100%;" />');						
				}
				
				if ( settings.debug )
				{
					$(this)
						.css('background', 'rgba(255,0,0,0.25)'); // for testing
				}
				
			})
    	.on('equalLines.updateLayout', function() {
	    	
        var
        	self			  = $(this),
					wrapper     = self.find('[data-equal-lines-wrapper]'),
	    		oldHeight   = 0,
	    		renderCount = 0;
	    		
	    	if ( ! self.data('equalLines.initialized') ) 
	    	{
  	    	return;
	    	}
	    	
	    	if ( self.css('display') === 'inline' )
	    	{
  	    	console.log('jQuery Equal Lines doesn\'t work on "display: inline" elements! Setting elements to "inline-block"');
  	    	self
  	    	  .css('display', 'inline-block');
	    	}
	    		
	    	// reset the wrapper
	    	
	    	wrapper
	    	  .css('width', '100%');

				if ( settings.debug )
				{
					self
						.css('background', 'rgba(255,0,0,0.5)'); // for testing
				}	    	  

	    	// get the element's height
	    		
	    	oldHeight = self.height();
	    	
	    	// decrease the width until it breaks onto a new line
	    	
        while ( ( self.height() === oldHeight && self.height() && oldHeight ) || ( settings.debug && renderCount > 999 ) )	
	    	{
  	    	wrapper
  	    	  .width(wrapper.width() - Math.max(settings.precision, 1));
  	    	renderCount++;
	    	}
	    	
	    	if ( settings.debug )
	    	{
  	    	console.log('Finished:', self.height(), oldHeight, wrapper.width(), renderCount);
	    	}
	    	
	    	// now increase the width one increment
	    	
	    	wrapper
	    	  .width(wrapper.width() + Math.max(settings.precision, 1));

    	})
    	.trigger('equalLines.updateLayout');

		function throttle ($callback, $limit) 
		{
		  var 
		  	wait = false;    
		  
		  return function () 
		  {
		    if ( ! wait) 
		    { 
		      $callback.call();
		      wait = true;         
		      setTimeout(function () {
		      	wait = false; 
		      }, $limit);
		    
		    }
		    
		  };
		  
		}
		
		if ( elements.length )
		{
      $(window)
  	    .on('resize.equalLines', throttle(function() {
  		    
  				elements
  					.trigger('equalLines.updateLayout');
  					    
  	    }, Math.max(settings.throttling, 0)));    	 
		}
	    
    return this;
  };
 
}( jQuery ));

